import {HEADER_GET} from "../../actions/actionTypes.js";

const initState = {
    logo: '',
    menu: ''
};

function headerReducer(state = initState, action) {
    switch (action.type) {
        case HEADER_GET:
            return Object.assign({}, state, {
                logo: action.payLoad.logo.url,
                menu: action.payLoad.menu
            });
        default:
            return state;
    }
}

export default headerReducer;