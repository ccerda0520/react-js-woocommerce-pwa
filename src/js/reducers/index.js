import postsReducer from './postsReducer/index.js';
import headerReducer from './headerReducer/index.js';
import pageReducer from './pageReducer/index.js';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
    postsState: postsReducer,
    headerState: headerReducer,
    pageState: pageReducer
});

export default rootReducer;

