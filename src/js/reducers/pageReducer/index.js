import {PAGE_GET} from "../../actions/actionTypes.js";

const initState = {
    page: ''
};

function pageReducer(state = initState, action) {
    switch (action.type) {
        case PAGE_GET:
            return Object.assign({}, state, {
                page: action.payLoad.page
            });
        default:
            return state;
    }
}

export default pageReducer;