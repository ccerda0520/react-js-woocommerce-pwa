import { POSTS_GET } from '../../actions/actionTypes.js';
const initState = {
  posts: []
};

function postsReducer(state = initState, action) {
    switch (action.type) {
        case POSTS_GET: {
            return Object.assign({}, state, {
                posts: action.payLoad.posts
            });
        }
        default:
            return state;
    }
}

export default postsReducer;