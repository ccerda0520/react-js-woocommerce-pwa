import { POSTS_GET } from './actionTypes.js';
import { MENU_GET } from "./actionTypes.js";
import WpApiClient from '../service/wp-api-client/index.js';
import {HEADER_GET} from "./actionTypes";
import {PAGE_GET} from "./actionTypes";

function getPostsSuccess(posts)
{
    return {
        type: POSTS_GET,
        payLoad: {
            posts: posts
        }
    };
}
export function getPosts() {
    return function(dispatch) {
        WpApiClient.reduxGet('wp/v2/posts?_embed', dispatch, getPostsSuccess);
    }
}

function getHeaderSuccess(header)
{
    return {
        type: HEADER_GET,
        payLoad: {
            menu: header.menu,
            logo: header.logo
        }
    }
}

export function getHeader(id = 1) {
    return function(dispatch) {
        WpApiClient.reduxGetHeader(id, dispatch, getHeaderSuccess);
    }
}

function getPageSuccess(page)
{
    return {
        type: PAGE_GET,
        payLoad: {
            page: page
        }
    }
}

export function getPage(slug) {
    return function(dispatch) {
        WpApiClient.reduxGet(`cbc/v1/page?slug=${slug}`, dispatch, getPageSuccess);
    }
}