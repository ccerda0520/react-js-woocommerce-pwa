import React, {Component} from 'react';
import Header from './component/header/index.jsx';
import Footer from './component/footer/index.jsx';
import {Switch, Route} from 'react-router-dom';
import PageRouter from './page/pageRouter.jsx';
class App extends Component {
    constructor(props)
    {
        super(props);
    }

    render() {
        return (
            <div className="App">
                <Header />
                <PageRouter />
                <Footer />
            </div>
        );
    }
}

export default App;