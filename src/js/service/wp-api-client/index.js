export default class WpApiClient {
    static WPURL = 'https://woo-wp-api.dev.localhost/wp-json/';
    static AUTH_TOKEN = '';

    static get(url) {
        fetch(this.WPURL + url, {
            method: 'get'
        })
            .then(res => res.json())
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    }

    static reduxGet(url, dispatch, dispatchFunction) {
        fetch(this.WPURL + url, {
            method: 'get'
        })
            .then(res => res.json())
            .then(res => {
                try {
                    return dispatch(dispatchFunction(res));
                } catch(err) {
                }

            })
            .catch(err => {
                console.log(err);
                return err;
            });
    }

    static reduxGetMenu(id, dispatch, dispatchFunction) {
        this.reduxGet('cbc/v1/menu?id=' + id, dispatch, dispatchFunction);
    }

    static reduxGetHeader(menu_id, dispatch, dispatchFunction) {
        this.reduxGet('cbc/v1/header?id=' + menu_id, dispatch, dispatchFunction);
    }

    static post(url) {
        /**
         * @TODO edit the logic for post requests
         * @type {FormData}
         */
        let formData = new FormData();
        formData.append('title', 'Change da title again');
        fetch(this.WPURL + url, {
            method: 'post',
            withCredentials: true,
            credentials: 'include',
            headers: {
                //'Content-Type': 'multipart/form-data',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJod…J9fX0.WwEWOLWtlF2DaaGVki5tt1mt2N9luLqxWSfzWWq4cVM'
            },
            body: formData
        })
            .then(res => res.json())
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    }

    static authenticate() {
        /**
         * @TODO function works to retrieve authentication, needs to allow username and password passed into args, also create a new User service for session management
         * @type {FormData}
         */
        let formData = new FormData();
        formData.append('username', 'ccerda');
        formData.append('password', 'pumas#55');
        fetch(this.WPURL + 'jwt-auth/v1/token', {
            method: 'post',
            body: formData
        })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                this.AUTH_TOKEN = res;
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    }
}