import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getPage} from "../../actions";

function mapDispatchToProps(dispatch) {
    return {
        getPage: (slug) => dispatch(getPage(slug))
    }
}

function mapStateToProps(state) {
    return {
        page: state.pageState.page
    }
}


class ConnectedHome extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getPage('home');
    }

    render() {
        const {page} = this.props;
        if (typeof page.id === 'undefined') {
            return (<div>loading</div>);
        }
        console.log(page);
        const banner = page.meta.banner_background === "image" ?
            'url(' + page.meta.banner_image + ')' :
            page.meta.banner_color;
        return (
            <div className="home">
                <div className="page-banner" style={{
                    background: banner
                }}>
                    <div className="banner-content" dangerouslySetInnerHTML={{__html: page.meta.banner_content}}/>
                </div>
                <div className="container content-wrapper">
                    <div className="page-description"
                         dangerouslySetInnerHTML={{__html: page.meta.description_paragraph}}/>
                </div>
            </div>
        )
    }
}

const Home = connect(mapStateToProps, mapDispatchToProps)(ConnectedHome);

export default Home;


