import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from "./home/index.jsx";
import Page from '../component/page/index.jsx';
export default function PageRouter()
{
    return(
        <div className="page-wrapper">
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/:slug' component={Page} />
            </Switch>
        </div>
    )
}