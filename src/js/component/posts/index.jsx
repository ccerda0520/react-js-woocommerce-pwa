import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getPosts} from "../../actions/index.js";

function mapDispatchToProps(dispatch) {

    return {
        getPosts: () => dispatch(getPosts())
    };
}

function mapStateToProps(state) {
    return {
        posts: state.postsState.posts
    };
}

/**
 * @todo Figure out how to run local state management, ex loading message, for when a redux thunk async call is being ran
 * and it is necessary for the state to be updated once
 */
class ConnectedPosts extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        const {posts} = this.props;
        if (!posts.length) {
            return <div>...loading</div>
        }
        return (
            <ul>
                {
                    posts.map(function (post, index) {
                        //const postObject = JSON.parse(post);
                        const id = post.id;
                        const title = post.title.rendered;
                        const author = post._embedded.author[0].name;
                        const featuredImage = post._embedded['wp:featuredmedia'] ? post._embedded['wp:featuredmedia'][0].source_url : '';
                        const categories = post._embedded['wp:term'] ? post._embedded['wp:term'][0] : '';
                        const content = post.content.rendered;
                        const dateModified = post.modified;
                        return (
                            <li key={id}>
                                {
                                    featuredImage ?
                                    <div className="featured-image">
                                        <img src={featuredImage}/>
                                    </div>
                                    : ''
                                }
                                <div className="author">By {author}</div>
                                <div className="last-modified">Last Updated: {dateModified}</div>
                                <div className='categories'>
                                    {
                                        categories ?
                                            categories.map(function (category, index){
                                                return (
                                                    <div key={category.id} className={`category ${category.slug}`}>{category.name}</div>
                                                )
                                            })
                                            : ''
                                    }
                                </div>
                                <h1 className='title'>{title}</h1>
                                <div className="content" dangerouslySetInnerHTML={{__html: content}} />
                            </li>
                        );
                    })
                }
            </ul>
        );
    }
}

const Posts = connect(mapStateToProps, mapDispatchToProps)(ConnectedPosts);

export default Posts;
