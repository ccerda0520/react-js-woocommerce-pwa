import React from 'react';

function Footer() {
    return (
      <div className='footer'>
          <div className="container">
              <div className="copyright p">Copyright © {(new Date()).getFullYear()} Component Based Carlos. All Rights Reserved.</div>
              <div className="acknowledgement p">
                  Frontend Powered By <span className="red">ReactJS</span> and Backend Powered By <span className="red">Wordpress REST API</span>
              </div>
          </div>
      </div>
    );
}
export default Footer;