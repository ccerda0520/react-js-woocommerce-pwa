import React, {Component} from 'react';
import {connect} from 'react-redux';
import MainMenu from './mainMenu.jsx';
import {getHeader} from "../../actions";
import {Link,NavLink} from 'react-router-dom';

function mapDispatchToProps(dispatch) {
    return {
        getHeader: (id) => dispatch(getHeader(id))
    }
}

function mapStateToProps(state) {
    return {
        menu: state.headerState.menu,
        logo: state.headerState.logo
    }
}

class ConnectedHeader extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.getHeader(31);
    }
    render() {
        const {logo, menu} = this.props;
        if(!menu.length || !logo.length) {
            return (<div>loading...</div>);
        }
        return (
            <div className='header container'>
                <div className="row">
                    <div className="header-wrapper clearfix">
                        <div className="header-left col-md-4">
                            <div className="logo-wrapper">
                                <Link to="/" ><img src={logo} className="logo"/></Link>
                            </div>
                        </div>
                        <div className="header-right col-md-8">
                            <MainMenu menu={menu} className="main-menu"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const Header = connect(mapStateToProps, mapDispatchToProps)(ConnectedHeader);

export default Header;