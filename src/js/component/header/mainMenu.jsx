import React, {Component} from 'react';
import {Link,NavLink} from 'react-router-dom';

export default function MainMenu(props) {
    const {menu} = props;
    return (
        <nav className="main-menu">
            {
                menu.map(function (menuItem, index) {
                    const {id,slug,title} = menuItem;
                    return(
                        <div className="menu-item" key={id}><NavLink
                            exact={true}
                            to={`/${slug}`}
                            activeClassName="active">{title}</NavLink></div>
                    )
                })
            }
        </nav>
    );
}