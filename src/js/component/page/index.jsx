import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getPage} from "../../actions";

function mapDispatchToProps(dispatch) {
    return {
        getPage: (slug) => dispatch(getPage(slug))
    }
}

function mapStateToProps(state) {
    return {
        page: state.pageState.page
    }
}

class ConnectedPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slug: (typeof props.location.pathname !== 'undefined') ? props.location.pathname.replace('/', '') : ''
        }
    }

    componentDidMount() {
        this.props.getPage(this.state.slug);
    }

    render() {
        const {page} = this.props;
        if (typeof page.id === 'undefined') {
            return (<div>loading</div>);
        }
        return (
            <div className={page.title}>
                <div className="container"
                     dangerouslySetInnerHTML={{__html: page.content}}/>
            </div>
        );
    }
}

const Page = connect(mapStateToProps, mapDispatchToProps)(ConnectedPage);

export default Page;