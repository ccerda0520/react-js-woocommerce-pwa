<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 6/21/2018
 * Time: 9:06 PM
 */

class HeaderRoute extends WP_REST_Controller
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( $this, 'register_routes' ) );
    }

    public function register_routes()
    {
        $version = "1";
        $this->namespace = 'cbc/v' . $version;
        $this->rest_base = 'header';
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => array($this, 'getHeader'),
                'args'  => array(

                )
            )
        ));
    }

    /**
     * Grabs the menu and logo of the site
     * @param WP_REST_Request $request
     * @return WP_Error|WP_REST_Response
     */
    public function getHeader($request)
    {
        $menuId = $request->get_param('id');
        $menu = wp_get_nav_menu_items($menuId);
        $customLogoId = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $customLogoId , 'full' );
        if(empty($menu) || empty($logo)) {
            return new WP_Error('code', __('message', 'text-domain'));
        }
        $newData = [];
        foreach ($menu as $item) {
            $title = $item->title;
            $id = $item->object_id;
            $slug = (get_post($id))->post_name;
            $newData['menu'][] = [
                'id' => $id,
                'title' => $title,
                'slug'  => $slug
            ];
        }
        $newData['logo']['url'] =  $logo[0];
        return new WP_REST_Response($newData, 200);
    }
}

// create instance
$headerRoute = new HeaderRoute();