<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 6/21/2018
 * Time: 9:06 PM
 */

class MenusRoute extends WP_REST_Controller
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( $this, 'register_routes' ) );
    }

    public function register_routes()
    {
        $version = "1";
        $this->namespace = 'cbc/v' . $version;
        $this->rest_base = 'menu';
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => array($this, 'getMenu'),
                'args'  => array(

                )
            )
        ));
    }

    /**
     * @param WP_REST_Request $request
     * @return WP_Error|WP_REST_Response
     */
    public function getMenu($request)
    {
        $menuId = $request->get_param('id');
        $menu = wp_get_nav_menu_items($menuId);
        $data = $menu;
        if(empty($data)) {
            return new WP_Error('code', __('message', 'text-domain'));
        }
        $newData = [];
        foreach ($data as $item) {
            $title = $item->title;
            $id = $item->object_id;
            $slug = (get_post($id))->post_name;
            $newData[] = [
                'id' => $id,
                'title' => $title,
                'slug'  => $slug
            ];
        }
        return new WP_REST_Response($newData, 200);
    }
}

// create instance
$menusRoute = new MenusRoute();