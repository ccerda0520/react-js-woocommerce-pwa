<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 7/4/2018
 * Time: 7:12 AM
 */

class PageRoute extends WP_REST_Controller
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( $this, 'register_routes' ) );
    }

    public function register_routes()
    {
        $version = "1";
        $this->namespace = 'cbc/v' . $version;
        $this->rest_base = 'page';
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => array($this, 'getMenu'),
                'args'  => array(

                )
            )
        ));
    }

    /**
     * @param WP_REST_Request $request
     * @return WP_Error|WP_REST_Response
     */
    public function getMenu($request)
    {
        $slug = $request->get_param('slug');
        $page = get_page_by_path($slug,  OBJECT, 'page');
        if(empty($page)) {
            return new WP_Error('code', __('message', 'text-domain'));
        }
        $data = [];
        $data['id'] = $page->ID;
        $data['title'] = $page->post_title;
        $data['author'] = $page->post_author;
        $data['content'] = $page->post_content;
        $data['meta'] = function_exists('get_fields') ? get_fields($page->ID) : '';
        return new WP_REST_Response($data, 200);
    }
}

// create instance
$pageRoute = new PageRoute();