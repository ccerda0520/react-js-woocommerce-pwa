<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 6/21/2018
 * Time: 9:06 PM
 */

class LogoRoute extends WP_REST_Controller
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( $this, 'register_routes' ) );
    }

    public function register_routes()
    {
        $version = "1";
        $this->namespace = 'cbc/v' . $version;
        $this->rest_base = 'logo';
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => array($this, 'getLogo'),
                'args'  => array(

                )
            )
        ));
    }

    /**
     * @param WP_REST_Request $request
     * @return WP_Error|WP_REST_Response
     */
    public function getLogo($request)
    {
        $customLogoId = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $customLogoId , 'full' );
        $data = [
            'logo' => $logo[0]
        ];
        if(empty($data)) {
            return new WP_Error('code', __('message', 'text-domain'));
        }
        return new WP_REST_Response($data, 200);
    }
}

// create instance
$logoRoute = new LogoRoute();