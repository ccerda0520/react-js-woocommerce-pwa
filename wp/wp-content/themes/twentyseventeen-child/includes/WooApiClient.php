<?php
/**
 * Created by PhpStorm.
 * User: ccerda
 * Date: 5/2/2018
 * Time: 3:12 PM
 */

class WooApiClient
{
    protected static $instance;
    protected static $client;
    protected function __construct()
    {
    }
    public static function instance()
    {
        if(!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
}