<?php
// Include all files in the includes directory
foreach (glob(get_stylesheet_directory() . '/includes/**/*.php') as $filename)
{
    /** @noinspection PhpIncludeInspection */
    require($filename);
}

add_action('admin_enqueue_scripts', 'custom_admin_css') ;

function custom_admin_css() {
    wp_enqueue_style('custom-admin-css', get_stylesheet_directory_uri() . '/assets/css/admin.css', []);
}